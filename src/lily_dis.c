#include "lily.h"

#include "lily_dis_code_iter.h"
#include "lily_dis_opcode.h"
#include "lily_dis_bindings.h"

static void dis(lily_msgbuf *msgbuf, uint16_t *buffer, uint16_t *pos,
        const char *str)
{
    char temp[64];

    *pos = *pos + 1;
    sprintf(temp, "    [%4.d] %-9s%d\n", *pos, str, buffer[*pos]);
    lily_mb_add(msgbuf, temp);
}

#define ADD_FMT(...) sprintf(fmt_buffer, __VA_ARGS__); lily_mb_add(msgbuf, fmt_buffer);

#define T_VALUE   ADD_FMT("    [%4.d] value:   %d\n",  pos, (int16_t)buffer[pos]);
#define T_INPUT   ADD_FMT("    [%4.d] <------  #%d\n", pos, buffer[pos]);
#define T_OUTPUT  ADD_FMT("    [%4.d] ======>  #%d\n", pos, buffer[pos]);
#define T_JUMP    ADD_FMT("    [%4.d] +>    |  [%d] (%d)\n", pos, ci.offset + (int16_t)buffer[pos], (int16_t)buffer[pos]);
#define T_G_IN    ADD_FMT("    [%4.d] <------  #%d (G)\n", pos, buffer[pos]);
#define T_G_OUT   ADD_FMT("    [%4.d] ======>  #%d (G)\n", pos, buffer[pos]);
#define T_U_IN    ADD_FMT("    [%4.d] <------  #%d (up)\n", pos, buffer[pos]);
#define T_U_OUT   ADD_FMT("    [%4.d] ======>  #%d (up)\n", pos, buffer[pos]);

static void dump_code(lily_msgbuf *msgbuf, lily_function_val *fv)
{
    int first_pass = 1;
    char fmt_buffer[64];
    lily_dis_code_iter ci;
    uint16_t bytecode_len;
    uint16_t *bytecode = lily_function_bytecode(fv, &bytecode_len);

    lily_dis_ci_init(&ci, bytecode, 0, bytecode_len);

    while (lily_dis_ci_next(&ci)) {
        uint16_t *buffer = ci.buffer;
        uint16_t pos = ci.offset;
        lily_dis_opcode op = ci.buffer[pos];
        int i;

        if (first_pass == 0)
            lily_mb_add(msgbuf, "\n");
        else
            first_pass = 0;

        lily_mb_add_fmt(msgbuf, "[%d-%d] (%d) %s\n", pos,
                pos + ci.round_total - 1, buffer[pos],
                opcode_names[buffer[pos]]);

        if (ci.special_1) {
            switch (op) {
                case o_jump_if:
                    dis(msgbuf, buffer, &pos, "truthy:");
                    break;
                case o_load_boolean:
                case o_load_byte:
                case o_load_integer:
                    dis(msgbuf, buffer, &pos, "value:");
                    break;
                case o_global_set:
                    pos++;
                    T_G_IN
                    break;
                case o_global_get:
                    pos++;
                    T_G_OUT
                    break;
                case o_call_register:
                    pos++;
                    T_INPUT
                    break;
                case o_call_foreign:
                case o_call_native:
                    dis(msgbuf, buffer, &pos, "func:");
                    break;
                case o_build_variant:
                case o_load_empty_variant:
                    dis(msgbuf, buffer, &pos, "variant:");
                    break;
                case o_exception_catch:
                case o_instance_new:
                    dis(msgbuf, buffer, &pos, "class:");
                    break;
                case o_load_readonly:
                    dis(msgbuf, buffer, &pos, "literal:");
                    break;
                case o_property_get:
                case o_property_set:
                    dis(msgbuf, buffer, &pos, "index:");
                    break;
                case o_closure_set:
                    pos++;
                    T_U_IN
                    break;
                case o_closure_get:
                    pos++;
                    T_U_OUT
                    break;
                case o_closure_new:
                    dis(msgbuf, buffer, &pos, "size:");
                    break;
                default:
                    pos += ci.special_1;
                    break;
            }
        }

        if (ci.counter_2)
            dis(msgbuf, buffer, &pos, "count:");

        for (i = 0;i < ci.inputs_3;i++) {
            pos++;
            T_INPUT
        }

        for (i = 0;i < ci.outputs_4;i++) {
            pos++;
            T_OUTPUT
        }

        for (i = 0;i < ci.jumps_5;i++) {
            pos++;
            T_JUMP
        }
    }
}

void lily_dis__dis(lily_state *s)
{
    lily_function_val *fv = lily_arg_function(s, 0);
    lily_msgbuf *msgbuf = lily_msgbuf_get(s);
    const char *result;

    if (lily_function_is_foreign(fv))
        result = "<foreign function>";
    else {
        dump_code(msgbuf, fv);
        result = lily_mb_raw(msgbuf);
    }

    lily_return_string(s, result);
}

LILY_DECLARE_DIS_CALL_TABLE
