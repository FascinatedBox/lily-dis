import sys

var lily_dir = sys.argv
                  .get(1)
                  .unwrap_or("")
var src_dir = __dir__ ++ "..\/src\/"

if sys.argv.size() != 2 ||
    (lily_dir.ends_with("lily") == false &&
     lily_dir.ends_with("lily\/") == false): {
    print("Usage: update_dis.lily <path-to-lily-dir>")
    sys.exit_failure()
}

lily_dir = lily_dir.rstrip("\/") ++ "\/src\/"

define read_lines(path: String): List[String]
{
    var f = File.open(path, "r")
    var lines: List[String] = []

    f.each_line(|l| l.encode().unwrap() |> lines.push )

    f.close()
    return lines
}

define fix_common(source: List[String]): List[String]
{
    return source.reject(|s| s.is_space() )
                 .map(|m| m.replace("lily_", "lily_dis_")
                           .replace("LILY_", "LILY_DIS_"))
}

define fix_opcodes(source: List[String]): List[String]
{
    source = fix_common(source)

    var opcode_data = source.select(|s| s.starts_with("    o_"))
                            .map(|m| m.lstrip(" ") )
    var opcode_names = opcode_data.map(|m| "    \"" ++ m.replace(",", "\",") )
    var opcodes = opcode_data.map(|m| "    " ++ m )

    var header = """\
#ifndef LILY_DIS_OPCODE_H
# define LILY_DIS_OPCODE_H
static const char *opcode_names[] =
{
%1};
typedef enum {
%2} lily_dis_opcode;
#endif
""".replace("%1", opcode_names.join())
   .replace("%2", opcodes.join())

    return [header]
}

define transform_file(source: String,
                      dest: String,
                      fn: Function(List[String] => List[String]))
{
    var f = File.open(source, "r")
    var lines: List[String] = []

    f.each_line(|l| l.encode().unwrap() |> lines.push )
    f.close()
    f = File.open(dest, "w")
    fn(lines)
        .each(|e| e |> f.write )
    f.close()
}

var targets = [
    <["lily_code_iter.c", fix_common]>,
    <["lily_code_iter.h", fix_common]>,
    <["lily_opcode.h",    fix_opcodes]>,
]

for i in 0...targets.size() - 1: {
    var t = targets[i]
    var dest_name = t[0].replace("lily_", "lily_dis_")
    var src = lily_dir ++ t[0]
    var dest = src_dir ++ dest_name

    transform_file(src, dest, t[1])
    print("Updated " ++ dest_name ++ ".")
}
